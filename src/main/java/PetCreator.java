import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.UUID;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class PetCreator
{
    Scanner strings = new Scanner(System.in);
    Scanner nums = new Scanner(System.in);
    String inKey = Colours.BLUE + "\n)> " + Colours.RESET;

    /**
     * Method for creating pet
     * @param selectedOwnerID Which owner this pet will be linked to
     * @return Returns the pet to be added to HashMap
     */
    public Pet createPet(UUID selectedOwnerID)
    {
        String type;
        String name;
        String breed;
        String gender;

        int age;

        ArrayList<String> colours = new ArrayList();

        type = selectPetType();

        name = selectName();

        gender = selectGender();
        breed = selectBreed();

        age = selectAge();
        selectColours(colours);

        if(type.equals(Enums.PetType.MAMMAL.toString()))
        {
            boolean neutered = selectNeutered();

            Pet mammal = new Mammal(neutered, type, name, breed, gender, colours, age, selectedOwnerID);
            return mammal;
        }

        else if(type.equals(Enums.PetType.BIRD.toString()))
        {
            double wingspan = selectWingspan();
            boolean canFly = selectCanFly();

            Pet bird = new Bird(wingspan, canFly, type, name, breed, gender, colours, age, selectedOwnerID);
            return bird;
        }

        else
        {
            String waterType;
            System.out.print("Please enter your fish watertype" + inKey);
            waterType = strings.nextLine();

            Pet fish = new Fish(waterType, type, name, breed, gender, colours, age, selectedOwnerID);
            return fish;
        }
    }

    /**
     * Get user input for which type of pet it is
     * @return selected pet type
     */
    private String selectPetType()
    {
        int numInput = 0;
        boolean loop = true;
        Enums.PetType petType;

        while(loop)
        {
            try
            {
                System.out.println("Pet types:");
                for (int i = 0; i < Enums.PetType.values().length; i++)
                {
                    System.out.println("\t" + Colours.BLUE + (i + 1) + ". " + Enums.PetType.values()[i].toString() + Colours.RESET);
                }
                System.out.print("Enter a number to select your pet type" + inKey);

                numInput = nums.nextInt();
                numInput--;
                if (numInput < 0 || numInput > Enums.Menu.values().length - 1)
                {
                    System.out.println(Colours.PURPLE + "\nInvalid input, please enter a number between 1 and " +
                            Enums.Menu.values().length + Colours.RESET);
                    nums.nextLine();
                } else loop = false;
            }
            catch(InputMismatchException e)
            {
                System.out.println(Colours.PURPLE + "\nInvalid input, please enter a number between 1 and " +
                        Enums.Menu.values().length + Colours.RESET);
                nums.nextLine();
            }
        }

        petType = Enums.PetType.values()[numInput];

        switch(petType)
        {
            case MAMMAL:
                return Enums.PetType.MAMMAL.toString();
            case BIRD:
                return Enums.PetType.BIRD.toString();
            case FISH:
                return Enums.PetType.FISH.toString();
        }
        return null; //Should never reach this
    }

    /**
     * Get user input for pet's name
     * @return Pets name
     */
    public String selectName()
    {
        String name;
        while(true)
        {
            System.out.print("Enter the pet's name" + inKey);
            name = strings.nextLine();
            if(name.matches("[a-zA-Z\\s]+"))
            {
                return name;
            }
            System.out.println(Colours.PURPLE + "Name can only contain characters A-Z, no digits or special characters" + Colours.RESET);
        }
    }

    /**
     * Get user input for pet's gender
     * @return pet's gender
     */
    public String selectGender()
    {
        int numInput = 0;
        boolean loop = true;
        Enums.Gender petGender;

        while(loop)
        {
            try
            {
                ///Print all values in Gender Enum
                System.out.println("Pet Gender:");
                for (int i = 0; i < Enums.Gender.values().length; i++)
                {
                    System.out.println("\t" + (i + 1) + ". " + Enums.Gender.values()[i].toString());
                }
                System.out.print("Enter a number to select your pet's gender" + inKey);

                numInput = nums.nextInt();
                numInput--;
                if (numInput < 0 || numInput > Enums.Gender.values().length - 1)
                {
                    System.out.println(Colours.PURPLE + "\nInvalid input, please enter a number between 1 and " +
                            Enums.Gender.values().length + Colours.RESET);
                    nums.nextLine();
                } else loop = false;
            }
            catch(InputMismatchException e)
            {
                System.out.println(Colours.PURPLE + "\nInvalid input, please enter a number between 1 and " +
                        Enums.Gender.values().length + Colours.RESET);
                nums.nextLine();
            }
        }
        petGender = Enums.Gender.values()[numInput];

        switch(petGender)
        {
            case MALE:
                return Enums.Gender.MALE.toString();
            case FEMALE:
                return Enums.Gender.FEMALE.toString();
            case OTHER:
                return Enums.Gender.OTHER.toString();
            case UNKNOWN:
                return Enums.Gender.UNKNOWN.toString();
        }
        return null; //Should never reach this
    }

    /**
     * Get user input for pet's breed
     * @return pet's breed
     */
    public String selectBreed()
    {
        String breed;
        while(true)
        {
            System.out.print("Enter the pet's breed" + inKey);
            breed = strings.nextLine();
            if(breed.matches("[a-zA-Z\\s]+"))
            {
                return breed;
            }
            System.out.println(Colours.PURPLE + "Breed can only contain characters A-Z, no digits or special characters" + Colours.RESET);
        }
    }

    /**
     * Get user input for pet's age
     * @return pet's age
     */
    public int selectAge()
    {
        int age;
        while(true)
        {
            try
            {
                System.out.print("Enter your pet's age (in years)" + inKey);
                age = nums.nextInt();
                if(age > 10000 || age < 0)
                {
                    System.out.println(Colours.PURPLE + "Age cannot be great than 10,000 or less than 0" + Colours.RESET);
                    nums.nextLine();
                    continue;
                }
                return age;
            }
            catch(InputMismatchException e)
            {
                System.err.println(Colours.PURPLE + "Enter a numeric value that is also a whole number" + Colours.RESET);
                nums.nextLine();
            }
        }
    }

    /**
     * Get user input for pet's colours
     * @param colours
     */
    public void selectColours(ArrayList<String> colours)
    {
        String stringInput;
        boolean loop = true;
        while (loop)
        {
            System.out.print("Enter your pets colours (MAX 3 colours, 1 at a time, type 0 to finish selecting colours)" + inKey);
            stringInput = strings.nextLine();
            try
            {
                if (Integer.parseInt(stringInput) == 0)
                {
                    loop = false;
                } else
                {
                    System.out.println(Colours.PURPLE + "That is not a valid option, try again" + Colours.RESET);
                    continue;
                }
            } catch (NumberFormatException e)
            {
                if (stringInput.length() <= 8)
                {
                    colours.add(stringInput);
                } else
                    System.out.println(Colours.PURPLE + "Input text cannot be more than 8 characters, try again" + Colours.RESET);
            }

            if (colours.size() == 3)
            {
                loop = false;
            }
        }
    }

    /**
     * Get user input if pet is neutered
     * @return is pet Neutered
     */
    private boolean selectNeutered()
    {
        String stringInput;
        while(true)
        {
            System.out.print("Is your pet neutered (Yes/No)" + inKey);
            stringInput = strings.nextLine().toUpperCase();
            if (stringInput.charAt(0) == 'Y')
            {
                return true;
            }
            else if (stringInput.charAt(0) != 'N')
            {
                System.out.println(Colours.PURPLE + "Invalid input! Please enter \"yes\" or \"no\"" + Colours.RESET);
                continue;
            }
            return false;
        }
    }

    /**
     * Get user input for pet's wingspan
     * @return pet's wingspan
     */
    private double selectWingspan()
    {
        double wingspan;

        while(true)
        {
            try
            {
                System.out.print("Enter your birds wingspan in inches" + inKey);
                wingspan = nums.nextDouble();
                nums.nextLine();
                return wingspan;
            }
            catch(InputMismatchException e)
            {
                System.out.println(Colours.PURPLE + "Invalid input, please enter a numeric value" + Colours.RESET);
                nums.nextLine();
            }
        }
    }

    /**
     * Get user input if pet can fly
     * @return can the pet fly?
     */
    private boolean selectCanFly()
    {
        String stringInput;
        while(true)
        {
            System.out.print("Can the bird fly? (Yes/No)" + inKey);
            stringInput = strings.nextLine().toUpperCase();
            if (stringInput.charAt(0) == 'Y')
            {
                return true;
            }
            else if (stringInput.charAt(0) != 'N')
            {
                System.out.println(Colours.PURPLE + "Invalid input! Please enter \"yes\" or \"no\"" + Colours.RESET);
                continue;
            }
            return false;
        }
    }
}
