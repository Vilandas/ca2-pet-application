import java.util.Comparator;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class PetComparatorByID implements Comparator<Pet>
{

    @Override
    public int compare(Pet p1, Pet p2)
    {
        return p1.getPetID().compareTo(p2.getPetID());
    }
}
