import java.util.ArrayList;
import java.util.UUID;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class Mammal extends Pet
{
    private boolean neutered;

    public Mammal(boolean neutered, String type, String name, String breed, String gender, ArrayList<String> colours, int age, UUID ownerID)
    {
        super(type, name, breed, gender, colours, age, ownerID);
        this.neutered = neutered;
    }

    public String isNeutered()
    {
        if(neutered)
        {
            return "Yes";
        }
        return "No";
    }

    @Override
    public void printPet(String ownerName)
    {
        System.out.printf("%-15s %-25s %-8s %-18s %-8s %-24s %-5s %-16s %-16s %-10s", getName(), ownerName, getType(),
                getBreed(), getGender(), getColours(), getAge(), getDateRegistered(), getTimeRegistered(), isNeutered());
    }

    @Override
    public String toString()
    {
        return super.toString() + ", Neutered = " + neutered + '}';
    }
    
    
    
    
}
