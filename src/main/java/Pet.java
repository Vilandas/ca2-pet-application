import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.UUID;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class Pet implements Serializable
{
    private String type;
    private String name;
    private String breed;
    private String gender;
    private ArrayList<String> colours;
    private int age;
    private UUID petID;
    private UUID ownerID;
    private LocalDate dateRegistered;
    private LocalTime timeRegistered;

    private long serialVersionUID = 1L;

    public Pet(String type, String name, String breed, String gender, ArrayList<String> colours, int age, UUID ownerID)
    {
        this.type = type;
        this.name = name;
        this.breed = breed;
        this.gender = gender;
        this.colours = colours;
        this.age = age;
        this.petID = UUID.randomUUID();
        this.ownerID = ownerID;
        this.dateRegistered = LocalDate.now();
        this.timeRegistered = LocalTime.now();
    }

    public String getType()
    {
        return type;
    }


    public String getName()
    {
        return name;
    }
    public void setName(String name) {this.name = name;}


    public String getBreed()
    {
        return breed;
    }
    public void setBreed(String breed) {this.breed = breed;}


    public String getGender()
    {
        return gender;
    }
    public void setGender(String gender) {this.gender = gender;}


    public ArrayList<String> getColours()
    {
        return colours;
    }
    public void clearColours() {this.colours.clear();}


    public int getAge()
    {
        return age;
    }
    public void setAge(int age) {this.age = age;}


    public UUID getPetID()
    {
        return petID;
    }


    public UUID getOwnerID() { return ownerID; }


    public LocalDate getDateRegistered() { return dateRegistered; }


    public LocalTime getTimeRegistered() { return timeRegistered; }


    //For testing purposes
    public void timeAddHour(int hour)
    {
        timeRegistered = timeRegistered.plusHours(hour);
    }

    public void printPet(String ownerName)
    {
        System.out.printf("%-15s %-25s %-8s %-18s %-8s %-24s %-5s %-16s %-16s %-10s %-10s %-10s %-12s", name, ownerName, type, breed,
            gender, colours, age, dateRegistered, timeRegistered, "", "", "", "");
    }

    @Override
    public String toString()
    {
        return petID + "{" + type + ", Name = " + name + ", Breed = " + 
                breed + ", Gender = " + gender + ", Colours = " + colours +
                ", DateRegistered = " + dateRegistered + ", Age = " + age + ", OwnerID = " + ownerID;
    }
}
