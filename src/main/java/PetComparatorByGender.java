import java.util.Comparator;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class PetComparatorByGender implements Comparator<Pet>
{

    @Override
    public int compare(Pet p1, Pet p2)
    {
        return p1.getGender().compareTo(p2.getGender());
    }
}
