import java.util.ArrayList;
import java.util.UUID;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class Fish extends Pet
{
    private String waterType;

    public Fish(String waterType, String type, String name, String breed, String gender, ArrayList<String> colours, int age, UUID ownerID)
    {
        super(type, name, breed, gender, colours, age, ownerID);
        this.waterType = waterType;
    }

    @Override
    public void printPet(String ownerName)
    {
        System.out.printf("%-15s %-25s %-8s %-18s %-8s %-24s %-5s %-16s %-16s %-10s %-10s %-10s %-12s", getName(),
                ownerName, getType(), getBreed(), getGender(), getColours(), getAge(), getDateRegistered(),
                getTimeRegistered(), "", "", "", waterType);
    }

    @Override
    public String toString()
    {
        return super.toString() + "WterType = " + waterType + '}';
    }
}
