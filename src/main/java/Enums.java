/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class Enums
{
//    @Override
//    public String getEnumName(Enum enumName)
//    {
//        String result = "";
//        String[] array = enumName.name().split("(?=\\p{Upper})");
//        for(String str : array)
//        {
//            result += (str + " ");
//        }
//        result += ":";
//        return result;
//    }

    public enum Menu
    {
        QUIT("Quit application (saves data)"),
        CREATE_OWNER("Create an owner"),
        SELECT_OWNER("Select owner to create/view/modify pets"),
        DISPLAY_PETS("Display all pets"),
        SORT_PETS("Sort pet data"),
        DISPLAY_OWNERS("Display all owners"),
        DISPLAY_STATISTICS("Generates statistics from current data"),
        REGEN_TEST_DATA("Overwrites all data with data for testing. This will the close program (Requires Restart)"),
        DELETE_ALL_DATA("Remove owners.bin and pets.bin for testing purposes");
        
        private final String description;
        
        Menu(String description)
        {
            this.description = description;
        }

        public String getDesc()
        {
            return description;
        }
        
        @Override
        public String toString()
        {
            String name = name();
            name = name.replaceAll("_", " ");
            return name.charAt(0) + name.substring(1).toLowerCase();
        }
    }

    public enum OwnerMenu
    {
        BACK("Back to main menu"),
        CREATE_PET("Create a pet"),
        DISPLAY_PETS("Display all pets"),
        MODIFY_PET("Modify pet details"),
        MODIFY_OWNER("Modify owner details");

        private final String description;

        OwnerMenu(String description)
        {
            this.description = description;
        }
        public String getDesc() {return description;}

        @Override
        public String toString()
        {
            String name = name();
            name = name.replaceAll("_", " ");
            return name.charAt(0) + name.substring(1).toLowerCase();
        }
    }

    public enum SortMenu
    {
        BACK,
        DATE_AND_TIME,
        NAME,
        PET_ID,
        GENDER,
        AGE,
        ALL_PETS,
        ONLY_MAMMALS,
        ONLY_BIRDS,
        ONLY_FISH,
        ASCENDING_ORDER,
        DESCENDING_ORDER;

        @Override
        public String toString()
        {
            String name = name();
            name = name.replaceAll("_", " ");
            return name.charAt(0) + name.substring(1).toLowerCase();
        }
    }

    public enum PetModifyMenu
    {
        BACK,
        CHANGE_NAME,
        CHANGE_BREED,
        CHANGE_GENDER,
        CHANGE_COLOURS,
        CHANGE_AGE;

        @Override
        public String toString()
        {
            String name = name();
            name = name.replaceAll("_", " ");
            return name.charAt(0) + name.substring(1).toLowerCase();
        }
    }

    public enum OwnerModifyMenu
    {
        BACK,
        CHANGE_NAME,
        CHANGE_EMAIL,
        CHANGE_PHONE_NUMBER,
        CHANGE_HOME_ADDRESS;

        @Override
        public String toString()
        {
            String name = name();
            name = name.replaceAll("_", " ");
            return name.charAt(0) + name.substring(1).toLowerCase();
        }
    }

    public enum PetType
    {
        MAMMAL,
        BIRD,
        FISH;

        public String enumName()
        {
            return "Pet Type: ";
        }

        @Override
        public String toString()
        {
            return name().charAt(0) + name().substring(1).toLowerCase();
        }
    }

    public enum Gender
    {
        MALE,
        FEMALE,
        OTHER,
        UNKNOWN;

        public String enumName()
        {
            return this.name();
        }
        
        @Override
        public String toString()
        {
            return name().charAt(0) + name().substring(1).toLowerCase();
        }
    }
}
