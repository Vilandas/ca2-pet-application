import java.io.IOException;
import java.util.NoSuchElementException;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class Main
{
    public static void main(String[] args)
    {
        Menu logic = new Menu();
        try
        {
            logic.menu();
        }
        catch(NoSuchElementException e)
        {
            System.out.println("Error: CTRL+D entered, which ends the system input");
            System.out.println("Attempting save: ");
            try
            {
                logic.ownerHandler.owners.saveOwnerData();
                logic.petHandler.pets.savePetData();
            } catch (IOException ex)
            {
                System.out.println(Colours.RED+"Error: Could not save data" + Colours.RESET);
            }
        }
        System.out.println("Closing program: ....");
    }
}
