import java.util.Scanner;
import java.util.regex.Pattern;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class OwnerCreator
{
    private Scanner strings = new Scanner(System.in);
    private String inKey = Colours.BLUE + "\n)> " + Colours.RESET;

    public Owner createOwner()
    {
        String name = selectName();
        String email = selectEmail();
        String phone = selectEmail();
        String address = selectAddress();

        return new Owner(name, email, phone, address);
    }

    public String selectName()
    {
        String name;
        while(true)
        {
            System.out.print("\nEnter owner's name" + inKey);
            name = strings.nextLine();
            if(name.matches("[a-zA-Z\\s]+"))
            {
                return name;
            }
            System.out.println(Colours.PURPLE + "Name can only contain characters A-Z, no digits or special characters" + Colours.RESET);
        }
    }

    //Regex referenced from https://howtodoinjava.com/regex/java-regex-validate-email-address/
    public String selectEmail()
    {
        String email = "";
        String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
        Pattern pattern = Pattern.compile(regex);
        boolean loop = true;
        while(loop)
        {
            System.out.print("\nEnter email address" + inKey);
            email = strings.nextLine();
            if(!pattern.matcher(email).matches())
            {
                System.out.println("Not a valid email");
                System.out.println("A valid email address can only contain letters, numbers, 1 @ symbol and a domain of 2-6 characters.");
                System.out.println("Example: myEmail541@gmail.ie" );
                System.out.println("                          ^^ = Domain");
                continue;
            }

            loop = false;
            for(Owner owner : OwnerHandler.owners.values())
            {
                if(owner.getEmail().equalsIgnoreCase(email))
                {
                    System.out.println("A user is already associated with this email");
                    loop = true;
                    break;
                }
            }
        }
        return email;
    }

    public String selectPhoneNumber()
    {
        String phone;
        while(true)
        {
            System.out.print("\nEnter your phone number" + inKey);
            phone = strings.nextLine();
            if(phone.matches("\\d+"))
            {
                return phone;
            }
            System.out.println(Colours.PURPLE + "Phone number can only contain numeric numbers (No decimal points)" + Colours.RESET);
        }
    }

    public String selectAddress()
    {
        System.out.print("\nEnter your home address" + inKey);
        return strings.nextLine();
    }
}
