import java.io.*;
import java.util.*;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class Pets implements Map<UUID, Pet>
{
    private LinkedHashMap<UUID, Pet> pets = new LinkedHashMap();

    /**
     * Saves each pet as an object to the file pets.bin
     * @throws IOException
     */
    public void savePetData() throws IOException
    {
        try(ObjectOutputStream petsFile = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("pets.bin"))))
        {
            for (Pet pet : pets.values())
            {
                petsFile.writeObject(pet);
            }
        }
    }

    /**
     * Loads all pets from pets.bin and puts them into a LinkedHashMap
     */
    public void loadPetData()
    {
        try(ObjectInputStream petsFile = new ObjectInputStream(new BufferedInputStream(new FileInputStream("pets.bin"))))
        {
            boolean eof = false;
            while(eof == false)
            {
                try
                {
                    Pet pet = (Pet) petsFile.readObject();
                    pets.put(pet.getPetID(), pet);
                }
                catch(EOFException e)
                {
                    eof = true;
                }
            }
        }
        catch(ClassNotFoundException | IOException e)
        {
            System.out.println(Colours.PURPLE + "Could not load pets.txt file or does not exist.");
            System.out.println("If this is your first time running this program, this is fine. " +
            "If not, force close the program and seek assistance as to not overwrite any data." + Colours.RESET);
        }
    }

    @Override
    public int size()
    {
        return pets.size();
    }


    @Override
    public boolean isEmpty()
    {
        return pets.isEmpty();
    }


    @Override
    public boolean containsKey(Object key)
    {
        return pets.containsKey(key);
    }


    @Override
    public boolean containsValue(Object value)
    {
        return pets.containsValue(value);
    }


    @Override
    public Pet get(Object key)
    {
        return pets.get(key);
    }


    @Override
    public Pet put(UUID key, Pet value)
    {
        return pets.put(key, value);
    }


    @Override
    public Pet remove(Object key)
    {
        return pets.remove(key);
    }


    @Override
    public void putAll(Map<? extends UUID, ? extends Pet> m)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }


    @Override
    public void clear()
    {
        pets.clear();
    }


    @Override
    public Set<UUID> keySet()
    {
        return pets.keySet();
    }


    @Override
    public Collection<Pet> values()
    {
        return pets.values();
    }


    @Override
    public Set<Entry<UUID, Pet>> entrySet()
    {
        return pets.entrySet();
    }
}
