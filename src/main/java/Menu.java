import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class Menu
{
    PetHandler petHandler = new PetHandler();
    OwnerHandler ownerHandler = new OwnerHandler(petHandler);

    Scanner nums = new Scanner(System.in);

    String inKey = Colours.BLUE + "\n)> " + Colours.RESET;

    /**
     * Get user input for the main menu options
     */
    public void menu()
    {
        boolean loop = true;
        int input;
        Enums.Menu menuOption;
        
        while(loop)
        {
            printMenu();
            try
            {
                input = nums.nextInt();
                if(input < 0 || input > Enums.Menu.values().length - 1)
                {
                    System.out.println(Colours.PURPLE + "\nInvalid input, please enter a number between 0 and " + (Enums.Menu.values().length-1) + Colours.RESET);
                    nums.nextLine();
                    continue;
                }
                menuOption = Enums.Menu.values()[input];
                switch(menuOption)
                {
                    case CREATE_OWNER:
                        ownerHandler.createOwner();
                        break;
                    case SELECT_OWNER:
                        ownerHandler.selectOwner();
                        break;
                    case DISPLAY_PETS:
                        petHandler.displayAllPets(ownerHandler.owners);
                        break;
                    case SORT_PETS:
                        petHandler.sortMenu();
                        break;
                    case DISPLAY_OWNERS:
                        ownerHandler.displayOwners();
                        break;
                    case DISPLAY_STATISTICS:
                        generateStatistics();
                        break;
                    case DELETE_ALL_DATA:
                        deleteData();
                        loop = false;
                        break;
                    case REGEN_TEST_DATA:
                        generateTestData();
                    case QUIT:
                        try
                        {
                            OwnerHandler.owners.saveOwnerData();
                            PetHandler.pets.savePetData();
                            loop = false;
                        } catch(IOException ex)
                        {
                            System.out.println(Colours.RED + "Error: Could not save data" + Colours.RESET);
                        }
                }
            }
            catch(InputMismatchException e)
            {
                System.out.println(Colours.PURPLE + "\nInvalid input, please enter a number between 0 and " + (Enums.Menu.values().length - 1) + Colours.RESET);
                nums.nextLine();
            }
        }
    }

    /**
     * Display menu options
     */
    private void printMenu()
    {
        System.out.println("\nOptions to select:");
        for(int i = 0 ; i < Enums.Menu.values().length; i++)
        {
            System.out.println("\t" + Colours.BLUE + i + ". " + Enums.Menu.values()[i].getDesc() + Colours.RESET);
        }
        System.out.print("Enter a number to select an option" + inKey);
    }

    /**
     * Generates some general average statistics
     */
    private void generateStatistics()
    {
        int petCount = petHandler.pets.size();
        int mammalCount = 0;
        int birdCount = 0;
        int fishCount = 0;

        int neuteredCount = 0;
        double averageWingSpan = 0;
        int birdCanFly = 0;

        for(Pet pet : petHandler.pets.values())
        {
            if(pet.getType().equals(Enums.PetType.MAMMAL.toString()))
            {
                mammalCount++;
                if(((Mammal) pet).isNeutered().equalsIgnoreCase("Yes"))
                {
                    neuteredCount++;
                }
            }
            else if(pet.getType().equals(Enums.PetType.BIRD.toString()))
            {
                birdCount++;
                averageWingSpan += ((Bird) pet).getWingspan();
                if(((Bird) pet).canFly().equalsIgnoreCase("Yes"))
                {
                    birdCanFly++;
                }
            }
            else if(pet.getType().equals(Enums.PetType.FISH.toString()))
            {
                fishCount++;
            }
        }
        if(petCount != 0)
        {
            if (birdCount != 0)
            {
                averageWingSpan /= birdCount;
            }

            System.out.println("\n****************************************************************************************");
            System.out.printf("%-30s %s", "Number of Pets:", petCount);
            System.out.printf("\n%-30s %d (%.2f %%)", "Number of Mammals:", mammalCount, ((double) mammalCount / petCount) * 100);
            System.out.printf("\n%-30s %d (%.2f %%)", "Number of Birds:", birdCount, ((double) birdCount / petCount) * 100);
            System.out.printf("\n%-30s %d (%.2f %%)", "Number of Fish:", fishCount, ((double) fishCount / petCount) * 100);
            System.out.printf("\n\n%-30s %d (%.2f %%)", "Neutered Mammals:", neuteredCount, getAverage(neuteredCount, mammalCount));
            System.out.printf("\n\n%-30s %.2f Inches", "Average bird wingspan:", averageWingSpan);
            System.out.printf("\n%-30s %d (%.2f %%)", "Birds that can fly:", birdCanFly, getAverage(birdCanFly, birdCount));
        }
        else System.out.println(Colours.PURPLE + "Error: No pets" + Colours.RESET);
        System.out.println();
    }

    /**
     *
     * @param a
     * @param b Cannot be zero (Cannot divide by zero)
     * @return a/b or 0 if b is 0
     */
    private double getAverage(double a, double b)
    {
        return b == 0 ? 0 : (a/b) * 100;
    }

    //For testing purposes only
    private void generateTestData()
    {
        PetHandler.pets.clear();
        ownerHandler.owners.clear();

        UUID vil = UUID.randomUUID();
        UUID john = UUID.randomUUID();
        UUID jac = UUID.randomUUID();
        ownerHandler.owners.put(vil, new Owner("Vilandas Morrissey", "Vil@gmail.com",
                "0858123", "40 Street at home"));
        ownerHandler.owners.put(john, new Owner("John Wick", "JW@under.com",
                "0858666", "The hub"));
        ownerHandler.owners.put(jac, new Owner("Jacob Daniels", "dan@gmail.com",
                "0858111", "20 O'Donald Street"));
        ownerHandler.owners.get(vil).setID(vil);
        ownerHandler.owners.get(john).setID(john);
        ownerHandler.owners.get(jac).setID(jac);

        petHandler.pets.put(UUID.randomUUID(), new Mammal(false, "Mammal", "Balto", "Border-Husky", "Male",
                (new ArrayList<String>(Arrays.asList("Black","White"))), 2, vil));
        petHandler.pets.put(UUID.randomUUID(), new Mammal(false,"Mammal", "Izzy", "Alaskan-Something", "Female",
                (new ArrayList<String>(Arrays.asList("Brown","White"))), 0, vil));
        petHandler.pets.put(UUID.randomUUID(), new Bird(29, true, "Bird", "Ivy", "Galah", "Female",
                (new ArrayList<String>(Arrays.asList("Pink","Gray"))), 4, vil));
        petHandler.pets.put(UUID.randomUUID(), new Fish("Tropical freshwater", "Fish", "Cray", "CrayFish", "Female",
                (new ArrayList<String>(Arrays.asList("White"))), 3, vil));

        petHandler.pets.put(UUID.randomUUID(), new Mammal(false, "Mammal", "Andy", "Beagle", "Male",
                (new ArrayList<String>(Arrays.asList("Black","Brown","White"))), 0, john));
        petHandler.pets.put(UUID.randomUUID(), new Mammal(false, "Mammal", "No", "Pitbull", "Male",
                (new ArrayList<String>(Arrays.asList("Dark-Grey","White"))), 3, john));

        petHandler.pets.put(UUID.randomUUID(), new Fish("Tropical freshwater", "Fish", "Danny", "Goldfish", "Male",
                (new ArrayList<String>(Arrays.asList("Orange"))), 1, jac));
        petHandler.pets.put(UUID.randomUUID(), new Bird(30, true, "Bird", "Crow", "Raven", "Female",
                (new ArrayList<String>(Arrays.asList("Black"))), 6, jac));

        int i = 0;
        for(Pet pet : petHandler.pets.values())
        {
            pet.timeAddHour(i++);
        }
    }

    /**
     * Delete owners.bin and pets.bin
     */
    private void deleteData()
    {
        File ownersFile = new File("owners.bin");
        File petsFile = new File("pets.bin");
        ownersFile.delete();
        petsFile.delete();
    }
}
