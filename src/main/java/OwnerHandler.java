import java.util.*;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class OwnerHandler
{
    public static Owners owners = new Owners();
    public OwnerCreator ownerCreator = new OwnerCreator();
    private Scanner strings = new Scanner(System.in);
    private Scanner nums = new Scanner(System.in);
    private String inKey = Colours.BLUE + "\n)> " + Colours.RESET;

    private PetHandler petHandler;

    /**
     * When OwnerHandler is created, it attempts to load owner data
     * and also places all owners into a LinkedHashSet for sorting
     * @param petHandler Reference from Menu for accessing PetHandler Methods
     */
    public OwnerHandler(PetHandler petHandler)
    {
        owners.loadOwnersData();
        this.petHandler = petHandler;
    }

    /**
     * Method that calls OwnerCreator to create an owner and then adds it to the owners HashMap
     */
    public void createOwner()
    {
        Owner owner = ownerCreator.createOwner();
        owners.put(owner.getID(), owner);
    }

    /**
     * Display all owners in a neat table
     */
    public void displayOwners()
    {
        System.out.println("\n****************************************************************************************");
        System.out.printf("%-25s %-20s %-14s %s", "Name:", "Email:", "Phone:", "Address:");
        for(Owner owner : OwnerHandler.owners.values())
        {
            System.out.println();
            owner.printOwner();
        }
        System.out.println("\n");
    }

    /**
     * Get user input for which owner they want to select and see details for or modify details of owner/pet
     */
    public void selectOwner()
    {
        int input = 1;
        while(input != 0)
        {
            System.out.println();
            int i = 0;
            System.out.println(Colours.BLUE + "\t0. Back to Menu" + Colours.RESET);
            for (Owner owner : owners.values())
            {
                System.out.println("\t" + Colours.BLUE + (++i) + ". " + owner.getName() + Colours.RESET);
            }
            System.out.print("Enter a number to select an option" + inKey);

            try
            {
                input = nums.nextInt();
                if (input < 0 || input > owners.values().size())
                {
                    System.out.println(Colours.PURPLE + "\nInvalid input, please enter a number between 0 and " + owners.values().size() + Colours.RESET);
                    nums.nextLine();
                } else if (input != 0)
                {
                    input--;
                    Owner owner = (Owner) (owners.values().toArray()[input]);
                    selectOwnerOptions(owner);
                    input = 0;
                }
            } catch (InputMismatchException e)
            {
                System.out.println(Colours.PURPLE + "\nInvalid input, please enter a number between 0 and " + owners.values().size() + Colours.RESET);
                nums.nextLine();
            }
        }

    }

    /**
     * Get user input for what option they want to select (Create pet, modify, display, etc)
     * @param owner Select owner
     */
    public void selectOwnerOptions(Owner owner)
    {
        boolean loop = true;
        int input;
        Enums.OwnerMenu menuOption;

        Map<Integer, Pet> ownerPets = new LinkedHashMap();
        petHandler.addPetsToOwnerArrayList(owner.getID(), ownerPets);

        while(loop)
        {
            System.out.println(owner.getName() + ":");
            displayOwnerOptions();
            try
            {
                input = nums.nextInt();
                if (input < 0 || input > Enums.OwnerMenu.values().length - 1)
                {
                    System.out.println(Colours.PURPLE + "\nInvalid input, please enter a number between 0 and " +
                            (Enums.OwnerMenu.values().length - 1) + Colours.RESET);
                    nums.nextLine();
                    continue;
                }
                menuOption = Enums.OwnerMenu.values()[input];
                switch (menuOption)
                {
                    case CREATE_PET:
                        petHandler.createPet(owner.getID());
                        break;
                    case DISPLAY_PETS:
                        petHandler.displayOwnerPets(ownerPets, owner.getName());
                        break;
                    case MODIFY_PET:
                        petHandler.selectOwnerPet(ownerPets, owner.getName());
                        break;
                    case MODIFY_OWNER:
                        ownerModifyMenu(owner);
                        break;
                    case BACK:
                        loop = false;
                        break;
                }
            } catch (InputMismatchException e)
            {
                System.out.println(Colours.PURPLE + "\nInvalid input, please enter a number between 0 and " +
                        (Enums.OwnerMenu.values().length - 1) + Colours.RESET);
                nums.nextLine();
            }
        }
    }

    /**
     * Display options after having selected an owner
     */
    public void displayOwnerOptions()
    {
        for(int i = 0 ; i < Enums.OwnerMenu.values().length; i++)
        {
            System.out.println("\t" + Colours.BLUE + i + ". " + Enums.OwnerMenu.values()[i].getDesc() + Colours.RESET);
        }
        System.out.print("Enter a number to select an option" + inKey);
    }

    /**
     * Get user input for what details the user want's to modify for the owner
     * @param owner The owner being modified
     */
    public void ownerModifyMenu(Owner owner)
    {
        boolean loop = true;
        int input;
        Enums.OwnerModifyMenu menuOption;
        while(loop)
        {
            displayOwnerModifyMenu();
            try
            {
                input = nums.nextInt();
                if (input < 0 || input > Enums.OwnerModifyMenu.values().length - 1)
                {
                    System.out.println(Colours.PURPLE + "\nInvalid input, please enter a number between 0 and " + (Enums.OwnerModifyMenu.values().length - 1) + Colours.RESET);
                    nums.nextLine();
                    continue;
                }
                menuOption = Enums.OwnerModifyMenu.values()[input];
                switch (menuOption)
                {
                    case CHANGE_NAME:
                        owner.setName(ownerCreator.selectName());
                        break;
                    case CHANGE_EMAIL:
                        owner.setEmail(ownerCreator.selectEmail());
                        break;
                    case CHANGE_PHONE_NUMBER:
                        owner.setPhone(ownerCreator.selectPhoneNumber());
                        break;
                    case CHANGE_HOME_ADDRESS:
                        owner.setAddress(ownerCreator.selectAddress());
                        break;
                    case BACK:
                        loop = false;
                }
            }
            catch(InputMismatchException e)
            {
                System.out.println(Colours.PURPLE + "\nInvalid input, please enter a number between 0 and " +
                        (Enums.OwnerModifyMenu.values().length - 1) + Colours.RESET);
                nums.nextLine();
            }
        }
    }

    /**
     * Displays menu for modifying owner details
     */
    public void displayOwnerModifyMenu()
    {
        System.out.println("\nOptions to select:");
        for(int i = 0 ; i < Enums.OwnerModifyMenu.values().length; i++)
        {
            System.out.println("\t" + Colours.BLUE + i + ". " + Enums.OwnerModifyMenu.values()[i].toString() + Colours.RESET);
        }
        System.out.print("Enter a number to select option (enter 0 to cancel)" + inKey);
    }
}
