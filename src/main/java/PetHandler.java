import java.util.*;
import java.util.stream.Collectors;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class PetHandler
{
    public static Pets pets = new Pets();
    public PetCreator petCreator = new PetCreator();
    public Set<Pet> allPets = new LinkedHashSet();

    Scanner nums = new Scanner(System.in);
    String inKey = Colours.BLUE + "\n)> " + Colours.RESET;

    Enums.SortMenu sortType = Enums.SortMenu.DATE_AND_TIME;
    Enums.PetType petType = null;
    boolean descending = false;

    /**
     * When PetHandler is created, it attempts to load pet data
     * and also places all pets into a LinkedHashSet for sorting
     */
    public PetHandler()
    {
        pets.loadPetData();
        allPets.addAll(pets.values());
        sortMenuSwitch(Enums.SortMenu.DESCENDING_ORDER);
    }

    /**
     * Method that calls PetCreator to create a pet and then adds it to the pets HashMap
     * @param selectedOwnerID Owners UUID for linking the pet to the owner
     */
    public void createPet(UUID selectedOwnerID)
    {
        Pet pet = petCreator.createPet(selectedOwnerID);
        pets.put(pet.getPetID(), pet);
        allPets.add(pet);
    }

    /**
     * When in the owners menu, adds all of the owners pets to a separate LinkedHashMap so it isn't required to loop
     * through all the pets each time.
     * @param selectedOwnerID Pets that contain this UUID will be added to the separate HashMap
     * @param ownerPets The separate Map for the owner's pets
     */
    public void addPetsToOwnerArrayList(UUID selectedOwnerID, Map<Integer, Pet> ownerPets)
    {
        System.out.println();
        int i = 0;
        for(Pet pet : PetHandler.pets.values())
        {
            if(pet.getOwnerID().equals(selectedOwnerID))
            {
                ownerPets.put(++i, pet);
            }
        }


    }

    /**
     * Displays all owner's pets in a neat table
     * @param ownerPets Map of owner's pets
     * @param name Owner's name
     */
    public void displayOwnerPets(Map<Integer, Pet> ownerPets, String name)
    {
        System.out.println("\n****************************************************************************************");
        System.out.printf("%-2s %-15s %-25s %-8s %-18s %-8s %-24s %-5s %-16s %-16s %-10s %-10s %-10s %-12s", "", "Name:", "Owner:", "Type:", "Breed:",
                "Gender:", "Colours:", "Age:", "Date Registered:", "Time Registered:", "Neutered:", "Wingspan:", "Can Fly:", "Water-type:");

        for(Integer i : ownerPets.keySet())
        {
            System.out.print("\n" + i + ". ");
            ownerPets.get(i).printPet(name);
        }
        System.out.println("\n");
    }

    /**
     * Display all pets in a neat table
     * @param owners A Map of all the owners
     */
    public void displayAllPets(Owners owners)
    {
        System.out.println("\n****************************************************************************************");
        System.out.printf("%-15s %-25s %-8s %-18s %-8s %-24s %-5s %-16s %-16s %-10s %-10s %-10s %-12s", "Name:", "Owner:", "Type:", "Breed:",
                "Gender:", "Colours:", "Age:", "Date Registered:", "Time Registered:", "Neutered:", "Wingspan:", "Can Fly:", "Water-type:");
        System.out.println();

        if (petType == null)
        {
            for (Pet pet : allPets)
            {
                pet.printPet(owners.get(pet.getOwnerID()).getName());
                System.out.println();
            }
        }
        else
        {
            for(Pet pet : allPets)
            {
                if(pet.getType().equals(petType.toString()))
                {
                    pet.printPet(owners.get(pet.getOwnerID()).getName());
                    System.out.println();
                }
            }
        }
    }

    /**
     * Method to find out which pet the user wants to modify
     * @param ownerPets Map of all owner's pets
     * @param name Owner's name
     */
    public void selectOwnerPet(Map<Integer, Pet> ownerPets, String name)
    {
        int input = 1;
        while(input != 0)
        {
            displayOwnerPets(ownerPets, name);
            System.out.print("Enter a number to select a pet to modify (enter 0 to cancel)" + inKey);
            try
            {
                input = nums.nextInt();
                if (input < 0 || input > ownerPets.size())
                {
                    System.out.println("\nInvalid input, please enter a number between 0 and " + ownerPets.size());
                    nums.nextLine();
                    continue;
                } else if (input != 0)
                {
                    petModifyMenu(ownerPets.get(input));
                }
                input = 0;
            } catch (InputMismatchException e)
            {
                System.out.println("\nInvalid input, please enter a number between 0 and " + ownerPets.size());
                nums.nextLine();
            }
        }
    }

    /**
     * Select option for modifying selected pet
     * @param pet The pet currently being modified
     */
    public void petModifyMenu(Pet pet)
    {
        boolean loop = true;
        int input;
        Enums.PetModifyMenu menuOption;
        while(loop)
        {
            displayPetModifyMenu();
            try
            {
                input = nums.nextInt();
                if (input < 0 || input > Enums.PetModifyMenu.values().length - 1)
                {
                    System.out.println(Colours.PURPLE + "\nInvalid input, please enter a number between 0 and " + (Enums.PetModifyMenu.values().length - 1) + Colours.RESET);
                    nums.nextLine();
                    continue;
                }
                menuOption = Enums.PetModifyMenu.values()[input];
                switch (menuOption)
                {
                    case CHANGE_NAME:
                        pet.setName(petCreator.selectName());
                        break;
                    case CHANGE_AGE:
                        pet.setAge(petCreator.selectAge());
                        break;
                    case CHANGE_BREED:
                        pet.setBreed(petCreator.selectBreed());
                        break;
                    case CHANGE_COLOURS:
                        pet.clearColours();
                        petCreator.selectColours(pet.getColours());
                        break;
                    case CHANGE_GENDER:
                        pet.setGender(petCreator.selectGender());
                        break;
                    case BACK:
                        loop = false;
                }
            }
            catch(InputMismatchException e)
            {
                System.out.println(Colours.PURPLE + "\nInvalid input, please enter a number between 0 and " + (Enums.PetModifyMenu.values().length - 1) + Colours.RESET);
                nums.nextLine();
            }
        }
    }

    /**
     * Display options for modifying pet
     */
    public void displayPetModifyMenu()
    {
        System.out.println("\nOptions to select:");
        for(int i = 0 ; i < Enums.PetModifyMenu.values().length; i++)
        {
            System.out.println("\t" + Colours.BLUE + i + ". " + Enums.PetModifyMenu.values()[i].toString() + Colours.RESET);
        }
        System.out.print("Enter a number to select option (enter 0 to cancel)" + inKey);
    }

    /**
     * Select option for sort menu
     */
    public void sortMenu()
    {
        boolean loop = true;
        int input;
        Enums.SortMenu menuOption;

        while(loop)
        {
            System.out.println("Sort Options: ");
            displaySortOptions();
            try
            {
                input = nums.nextInt();
                if (input < 0 || input > Enums.SortMenu.values().length - 1)
                {
                    System.out.println("\nInvalid input, please enter a number between 0 and " + (Enums.SortMenu.values().length - 1));
                    nums.nextLine();
                    continue;
                }
                menuOption = Enums.SortMenu.values()[input];
                sortMenuSwitch(menuOption);
                loop = false;
            } catch (InputMismatchException e)
            {
                System.out.println("\nInvalid input, please enter a number between 0 and " + (Enums.OwnerMenu.values().length - 1));
                nums.nextLine();
            }
        }
    }

    /**
     * Applies the selected option effect
     * @param menuOption Selected option
     */
    private void sortMenuSwitch(Enums.SortMenu menuOption)
    {
        boolean loop = true;
        while(loop)
        {
            switch (menuOption)
            {
                case DATE_AND_TIME:
                    sortType = Enums.SortMenu.DATE_AND_TIME;
                    if(descending)
                    {
                        allPets = allPets.stream().sorted(new PetComparatorByDateAndTime().reversed()).collect(Collectors.toCollection(LinkedHashSet::new));
                    }
                    else allPets = allPets.stream().sorted(new PetComparatorByDateAndTime()).collect(Collectors.toCollection(LinkedHashSet::new));
                    break;

                case NAME:
                    sortType = Enums.SortMenu.NAME;
                    if(descending)
                    {
                        allPets = allPets.stream().sorted(new PetComparatorByName().reversed()).collect(Collectors.toCollection(LinkedHashSet::new));
                    }
                    else allPets = allPets.stream().sorted(new PetComparatorByName()).collect(Collectors.toCollection(LinkedHashSet::new));
                    break;

                case PET_ID:
                    sortType = Enums.SortMenu.PET_ID;
                    if(descending)
                    {
                        allPets = allPets.stream().sorted(new PetComparatorByID().reversed()).collect(Collectors.toCollection(LinkedHashSet::new));
                    }
                    else allPets = allPets.stream().sorted(new PetComparatorByID()).collect(Collectors.toCollection(LinkedHashSet::new));
                    break;

                case GENDER:
                    sortType = Enums.SortMenu.GENDER;
                    if(descending)
                    {
                        allPets = allPets.stream().sorted(new PetComparatorByGender().reversed()).collect(Collectors.toCollection(LinkedHashSet::new));
                    }
                    else allPets = allPets.stream().sorted(new PetComparatorByGender()).collect(Collectors.toCollection(LinkedHashSet::new));
                    break;

                case AGE:
                    sortType = Enums.SortMenu.AGE;
                    if(descending)
                    {
                        allPets = allPets.stream().sorted(new PetComparatorByAge().reversed()).collect(Collectors.toCollection(LinkedHashSet::new));
                    }
                    else allPets = allPets.stream().sorted(new PetComparatorByAge()).collect(Collectors.toCollection(LinkedHashSet::new));
                    break;

                case ALL_PETS:
                    petType = null;
                    break;

                case ONLY_MAMMALS:
                    petType = Enums.PetType.MAMMAL;
                    break;

                case ONLY_BIRDS:
                    petType = Enums.PetType.BIRD;
                    break;

                case ONLY_FISH:
                    petType = Enums.PetType.FISH;
                    break;

                case ASCENDING_ORDER:
                    if(descending)
                    {
                        descending = false;
                        sortMenuSwitch(sortType);
                    }
                    break;

                case DESCENDING_ORDER:
                    if(descending == false)
                    {
                        descending = true;
                        sortMenuSwitch(sortType);
                    }
                    break;

                case BACK:
                    break;
            }
            loop = false;
        }
    }

    /**
     * Display sort option with arrows pointing to currently selected options
     */
    public void displaySortOptions()
    {
        for(int i = 0 ; i < Enums.SortMenu.values().length; i++)
        {
            System.out.print("\n\t" + Colours.BLUE + i + ". " + Enums.SortMenu.values()[i].toString() + Colours.RESET);
            if(sortType == Enums.SortMenu.values()[i])
            {
                System.out.print(Colours.GREEN + "  <-------" + Colours.RESET);
            }
            else if(Enums.SortMenu.values()[i] == Enums.SortMenu.ASCENDING_ORDER && descending == false)
            {
                System.out.print(Colours.GREEN + "  <-------" + Colours.RESET);
            }
            else if(Enums.SortMenu.values()[i] == Enums.SortMenu.DESCENDING_ORDER && descending == true)
            {
                System.out.print(Colours.GREEN + "  <-------" + Colours.RESET);
            }
            else if(petType == null)
            {
                if(Enums.SortMenu.values()[i] == Enums.SortMenu.ALL_PETS)
                {
                    System.out.print(Colours.GREEN + "  <-------" + Colours.RESET);
                }
            }
            else if(Enums.SortMenu.values()[i].name().contains(petType.name()))
            {
                System.out.print(Colours.GREEN + "  <-------" + Colours.RESET);
            }
        }
        System.out.print("\nEnter a number to select an option" + inKey);
    }
}
