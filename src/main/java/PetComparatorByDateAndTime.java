import java.util.Comparator;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class PetComparatorByDateAndTime implements Comparator<Pet>
{

    @Override
    public int compare(Pet p1, Pet p2)
    {
        int flag = p1.getDateRegistered().compareTo(p2.getDateRegistered());
        if(flag == 0)
        {
            flag = p1.getTimeRegistered().compareTo(p2.getTimeRegistered());
        }
        return flag;
    }
}
