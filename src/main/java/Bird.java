import java.util.ArrayList;
import java.util.UUID;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class Bird extends Pet
{
    private double wingspan;
    private boolean canFly;

    public Bird(double wingspan, boolean canFly, String type, String name, String breed, String gender, ArrayList<String> colours, int age, UUID ownerID)
    {
        super(type, name, breed, gender, colours, age, ownerID);
        this.wingspan = wingspan;
        this.canFly = canFly;
    }

    public double getWingspan()
    {
        return wingspan;
    }

    public String canFly()
    {
        if(canFly)
        {
            return "Yes";
        }
        return "No";
    }

    @Override
    public void printPet(String ownerName)
    {
        System.out.printf("%-15s %-25s %-8s %-18s %-8s %-24s %-5s %-16s %-16s %-10s %-10s %-10s", getName(), ownerName,
                getType(), getBreed(), getGender(), getColours(), getAge(), getDateRegistered(), getTimeRegistered(),
                "", wingspan, canFly());
    }

    @Override
    public String toString()
    {
        return super.toString() + ", Wingspan = " + wingspan + ", canFly = " + canFly + '}';
    }
    
    
    
    
}
