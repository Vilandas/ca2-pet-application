import java.io.Serializable;
import java.util.UUID;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class Owner implements Serializable
{
    private String name;
    private String email;
    private String phone;
    private String address;
    private UUID ID;

    private long serialVersionUID = 2L;

    public Owner(String name, String email, String phone, String address)
    {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.ID = UUID.randomUUID();
    }

    public String getName()
    {
        return name;
    }
    public void setName(String name) {this.name = name;}

    public String getEmail() { return email; }
    public void setEmail(String email) {this.email = email;}

    public String getPhone()
    {
        return phone;
    }
    public void setPhone(String phone) {this.phone = phone;}

    public String getAddress()
    {
        return address;
    }
    public void setAddress(String address) {this.address = address;}

    public UUID getID()
    {
        return ID;
    }

    public void setID(UUID ID) {this.ID = ID;} //For testing purposes only

    public void printOwner()
    {
        System.out.printf("%-25s %-20s %-14s %s", name, email, phone, address);
    }

    @Override
    public String toString()
    {
        return "Owner{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", address='" + address + '\'' +
                ", ID=" + ID +
                '}';
    }
    
    
    
    
}
