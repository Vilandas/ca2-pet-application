import java.io.*;
import java.util.*;

/**
 *
 * @author Vilandas Morrissey D00218436
 */
public class Owners implements Map<UUID, Owner>
{
    private static Map<UUID, Owner> owners = new HashMap();
    
    public void saveOwnerData() throws IOException
    {
        try(ObjectOutputStream ownersFile = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("owners.bin"))))
        {
            for (Owner owner : owners.values())
            {
                ownersFile.writeObject(owner);
            }
        }
    }
    
    public void loadOwnersData()
    {
        try(ObjectInputStream ownersFile = new ObjectInputStream(new BufferedInputStream(new FileInputStream("owners.bin"))))
        {
            boolean eof = false;
            while(eof == false)
            {
                try
                {
                    Owner owner = (Owner) ownersFile.readObject();
                    owners.put(owner.getID(), owner);
                }
                catch(EOFException e)
                {
                    eof = true;
                }
            }
        }
        catch(ClassNotFoundException | IOException e)
        {
            System.out.println(Colours.PURPLE + "Could not load owners.bin file or does not exist.");
            System.out.println("If this is your first time running this program, this is fine. " +
                    "If not, force close the program and seek assistance as to not overwrite any data." + Colours.RESET);
        }
    }

    @Override
    public int size()
    {
        return owners.size();
    }


    @Override
    public boolean isEmpty()
    {
        return owners.isEmpty();
    }


    @Override
    public boolean containsKey(Object key)
    {
        return owners.containsKey(key);
    }


    @Override
    public boolean containsValue(Object value)
    {
        return owners.containsValue(value);
    }


    @Override
    public Owner get(Object key)
    {
        return owners.get(key);
    }


    @Override
    public Owner put(UUID key, Owner value)
    {
        return owners.put(key, value);
    }


    @Override
    public Owner remove(Object key)
    {
        return owners.remove(key);
    }


    @Override
    public void putAll(Map<? extends UUID, ? extends Owner> m)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }


    @Override
    public void clear()
    {
        owners.clear();
    }


    @Override
    public Set<UUID> keySet()
    {
        return owners.keySet();
    }


    @Override
    public Collection<Owner> values()
    {
        return owners.values();
    }


    @Override
    public Set<Entry<UUID, Owner>> entrySet()
    {
        return owners.entrySet();
    }






}
